# Ruggeri_arXiv_2112.10689_2022

Contains input files and data used to generate the figures of the article: 

Multi-scale simulation of the adsorption of lithium ion on graphite surface: from Quantum Monte Carlo to Molecular Density Functional Theory

Michele Ruggeri, Kyle Reeves, Tzu-Yao Hsu, Guillaume Jeanmairet, Mathieu Salanne, and Carlo Pierleoni, arXiv, 2112.10689

https://arxiv.org/abs/2112.10689

The folder *QMC-Input* contains input files for QMCPACK, which is available [here](https://qmcpack.org/)

The folder *DFT-Input*  contains input files for Quantum ESPRESSO, which is available [here](https://www.quantum-espresso.org/) 

The folder *MetalWalls-Input* contains input files for MetalWalls, which is available [here](https://gitlab.com/ampere2/metalwalls)

The file *QMCresults.dat* contains the benchmark QMC results obtained in this study
